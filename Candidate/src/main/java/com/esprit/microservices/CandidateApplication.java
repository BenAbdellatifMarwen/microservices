package com.esprit.microservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
public class CandidateApplication {

	public static void main(String[] args) {
		SpringApplication.run(CandidateApplication.class, args);
	}
	
	@Autowired
	private CandidatRepository repository ;
	
	@Bean
	ApplicationRunner init() {
		return(args) -> {
			repository.save(new Candidat("test1"));
			repository.save(new Candidat("test2"));
			repository.save(new Candidat("test3"));
			repository.save(new Candidat("test4"));
			repository.findAll().forEach(System.out::println);
		};
		
		
		
	}

}
