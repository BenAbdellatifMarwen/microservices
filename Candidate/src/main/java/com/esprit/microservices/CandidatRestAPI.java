package com.esprit.microservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/candidats")
public class CandidatRestAPI {
	
	private String title = "hello , i'm ...";
	@Autowired
	private CandidatService candidatservice;
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Candidat> createCandidat(@RequestBody Candidat candidat) {
		
		return new ResponseEntity<>(candidatservice.addCandidat(candidat), HttpStatus.OK);
	}
	@PutMapping(value = "/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
public  ResponseEntity<Candidat> updatecandidat(@PathVariable(value = "id") int id , @RequestBody Candidat candidat)
{
		return new ResponseEntity<>(candidatservice.updateCandidat(id, candidat), HttpStatus.OK);
}
	
	@DeleteMapping(value = "/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
public  ResponseEntity<String> Deletecandidat(@PathVariable(value = "id") int id)
{
		return new  ResponseEntity<>(candidatservice.deleteCandidat(id), HttpStatus.OK);
}
}
