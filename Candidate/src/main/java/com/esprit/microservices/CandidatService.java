package com.esprit.microservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidatService {
	@Autowired
	private CandidatRepository CandidatRepository;
	
	public Candidat addCandidat(Candidat candidat)
	{
		return CandidatRepository.save(candidat);
	}
	
	
	public Candidat updateCandidat(int id , Candidat newCandidat)
	{
		if(CandidatRepository.findById(id).isPresent()) {
			Candidat existingCandidat = CandidatRepository.findById(id).get();
			existingCandidat.setNom(newCandidat.getNom());
			existingCandidat.setPrenom(newCandidat.getPrenom());
			existingCandidat.setEmail(newCandidat.getEmail());
            return CandidatRepository.save(existingCandidat);
		} else
			return null;
	}
	
	public String deleteCandidat(int id)
	{
		if(CandidatRepository.findById(id).isPresent()) {
			CandidatRepository.deleteById(id);
			return "Candidat  suprome";
		} else
			return "Candidat non suprome";
	}
}
