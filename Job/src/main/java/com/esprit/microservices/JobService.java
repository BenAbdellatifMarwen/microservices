package com.esprit.microservices;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobService {

    List<Job> tutorials = new ArrayList<Job>();

	@Autowired
	private JobRepository JobRepository;
	
	
	public List<Job> afficheJob()
	{
		return JobRepository.findAll();

	}
	
	public Job updateCandidat(int id , Boolean etat)
	{
		if(JobRepository.findById(id).isPresent()) {
			Job existingJob = JobRepository.findById(id).get();
			existingJob.setEtat(etat);
            return JobRepository.save(existingJob);
		} else
			return null;
	}
	
	
	
	
}
