package com.esprit.microservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
public class JobApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobApplication.class, args);
	}

	
	@Autowired
	private JobRepository repository ;
	
	@Bean
	ApplicationRunner init() {
		return(args) -> {
			repository.save(new Job("test1", true));
			repository.save(new Job("test2", false));
			/*JS.afficheJob();
			JS.afficheJobparidname(1, "test1");
			JS.updateCandidat(1, false);
			JS.afficheJob();
			repository.findAll().forEach(System.out::println);*/


		};
		
		
		
	}

	
}
