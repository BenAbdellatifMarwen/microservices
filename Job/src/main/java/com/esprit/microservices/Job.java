package com.esprit.microservices;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Job implements Serializable {

	private static final long SerialVersionUID = 6515;

	@Id
	@GeneratedValue
	private int id;
	private String Service;
	private Boolean Etat;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getService() {
		return Service;
	}
	public void setService(String service) {
		Service = service;
	}
	public Boolean getEtat() {
		return Etat;
	}
	public void setEtat(Boolean etat) {
		Etat = etat;
	}
	public Job( String service, Boolean etat) {
		super();
		Service = service;
		Etat = etat;
	}
	public Job() {
		super();
	}
	
	
}
